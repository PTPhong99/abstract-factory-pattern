<?php
require_once "AnimalFactory.php";

$animal = new AnimalFactory();

$twoLegsAnimal = $animal->createAnimal(2, 'chicken,duck,bird..');
echo $twoLegsAnimal->getDescriptionAnimal();

$fourLegsAnimal = $animal->createAnimal(4, 'dog,cat,cow..');
echo $fourLegsAnimal->getDescriptionAnimal();