<?php
require_once "AbstractFactory.php";
require_once "TypeAnimal.php";

class AnimalFactory extends IsAnimal
{
    public function createAnimal($type, $discription)
    {
        if ($type === 2){
            return new AnimalTwoLegs($discription);
        }
        if ($type === 4){
            return new AnimalFourLegs($discription);
        }
        return null;
    }
}