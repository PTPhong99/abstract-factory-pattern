<?php

abstract class Description
{
    private $description;

    public function __construct($description)
    {
        $this->description = $description;
    }

    public function getDescriptionAnimal(){
        return $this->description;
    }
}

class AnimalTwoLegs extends TypeAnimal
{
    //code
}
class AnimalFourLegs extends TypeAnimal
{
    //code
}